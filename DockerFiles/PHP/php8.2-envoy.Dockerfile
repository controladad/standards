# Set the base image for subsequent instructions
FROM php:8.2

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install apt-utils -y
RUN apt-get install -y gnupg -qq git unzip wget curl libmcrypt-dev g++ libicu-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev libzip-dev zlib1g-dev
# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install pdo_mysql zip gd pdo exif calendar
RUN pecl install -n mcrypt
# RUN pecl install mcrypt-1.0.4 && docker-php-ext-enable mcrypt
RUN docker-php-ext-configure intl && docker-php-ext-install intl

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~2.0"
