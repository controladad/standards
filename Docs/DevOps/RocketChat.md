# Rocket.Chat

\+ jitsi meet

## Installation

make sure your docker-compose version is up to date as some older versions have trouble dealing with .env files

1. Download everything from the `Samples/rocketchat+jitsi` folder
2. copy `env.sample` into `.env` (`cp env.sample .env`) and configure everything to your needs
3. run `./gen-passwords.sh`
4. run `docker-compose up -d` and your server should be running

## Reverse Proxy (Nginx)

next you should setup a reverse proxy, here is the configuration for nginx

rocket chat:

```nginx
server {
        listen 443 ssl;

        ssl_certificate         /ssl/cert/for/your/domain;
        ssl_certificate_key     /ssl/cert/key/for/your/domain;

        server_name rocketchat.your.domain;
        location / {
                proxy_pass http://localhost:YOUR_CHOSEN_PORT;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $http_host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto https;
                proxy_set_header X-Nginx-Proxy true;
                proxy_redirect off;
        }
}
```

`YOUR_CHOSEN_PORT` is the same as `ROCKETCHAT_PORT` in your .env file

jitsi meet:

```nginx
server {
        listen 443 ssl;

        ssl_certificate         /ssl/cert/for/your/domain;
        ssl_certificate_key     /ssl/cert/key/for/your/domain;

        server_name jitsi.your.domain;
        location / {
                proxy_pass http://localhost:YOUR_CHOSEN_HTTP_PORT;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto https;
                proxy_set_header X-Nginx-Proxy true;
                proxy_redirect off;
        }
        location /xmpp-websocket {
                proxy_pass http://localhost:YOUR_CHOSEN_HTTP_PORT/xmpp-websocket;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $host;
                tcp_nodelay on;
       }
}
```

`YOUR_CHOSEN_HTTP_PORT` being the `HTTP_PORT` in your .env file NOT `HTTPS_PORT`
