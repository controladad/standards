# RabbitMQ
## Docker
To run RabbitMQ as dockerized system you can use this command to create your container

It will create a container and expose default ports of rabbitmq(5672, 15672) and also a username and password where you can use it to login into system

Fill username and password with your desired credentials

```bash
docker run -d --hostname my-rabbit --name some-rabbit -p 5672:5672 -p 15672:15672 -e RABBITMQ_DEFAULT_USER=<username> -e RABBITMQ_DEFAULT_PASS=<password> rabbitmq:3-management
```
