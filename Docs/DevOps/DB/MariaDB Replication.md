# MariaDB Replication

## Prerequisites
1. Mariadb-server and mariadb-client packages are installed.
    - You can install using the following commands:

        `sudo apt-get install mariadb-server mariadb-client`
2. Check bind-address and netwroking on MasterDB. You must have remote access for Master Server Mysql.
    - Set `bind-address` to `0.0.0.0` to allow remote access in `/etc/mysql/my.cnf` unser `[mysqld]` section.: 

        ```
        [mysqld]

        bind-address = 0.0.0.0
        ```

3. tmux on Master Server for easy life.

## Setup
### Step 1 - Enable binlog in Master DB

1. First you need to edit the `/etc/my.cnf` on the Master DB, and add these lines in the `[mariadb]` section.

    ```
    [mariadb]
    log-bin
    server_id=1
    log-basename=master1
    binlog-format=mixed
    binlog_do_db=<db_you_want_to_replicate>
    binlog-ignore-db=<db_you_dont_want_to_replicate for example mysql>
    ```

2. restart the Master DB to reflect the above configurations
    ```
    sudo systemctl restart mariadb
    ```

### Step 2 - Create Replication user on Master DB
1. Login into the Master DB and create a user with the following commands.
This user use for replication purpose and Slave DB will use this user to connect to Master DB.

    Fill the following details in the form below.

    `username` - This is the username of the replication user.

    `slave_host` - This is the IP Address of the Slave DB.

    `password` - This is the password of the replication user.

    ```
    CREATE USER '<username>'@'<slave_host>';
    GRANT REPLICATION SLAVE ON *.* TO '<username>'@'<slave_host>' IDENTIFIED BY '<password>';
    FLUSH PRIVILEGES;
    ```

### Step 3 - Transfer Master Data to Salve DB
There Two way for this manner first on is more reliable due to the fact that Master DB will be locked and we insure that no new data will be inserted into the Master DB, But it cause downtime on Master DB and related services.
The second way will skip locking tables but there's a chance that some last data will be lost and have breaking effect on Slave DB.

If the Master DB using in online/production server and you want to use first method you can clone the Master DB and then go with first method to transfer data, __BUT WE NEVER TEST IT BEFORE__!

### Method 1 - Locking Tables(Recommended and Reliable)

1. Run following command in the Master DB to lock all tables.

    ```
    FLUSH TABLES WITH READ LOCK;
    ```

2. Then obtain the binary log coordinates with the following command. The coordinates are the name of the binary log file currently in use, and the position of the last written event.

    ```
    SHOW MASTER STATUS;
    ```

    Note down the __File__ parameter and __Position__ parameter.


    Important: Do NOT exit MariaDB monitor yet, because exiting it now will __release the lock__. __Record File and Position details.__ Now open another terminal window and SSH into your master server. Use mysqldump utility to dump the database to a .sql file.

3. Create a dump of database
    ```
    sudo mysqldump -u root -p database_name > database_name.sql
    ```

4. After dumnp get completed, you can unlock the tables with the following command.

    ```
    UNLOCK TABLES;
    ```

### Method 2 - Skip Locking Tables(Not Recommended)
1. Create a dump of current Master DB with following command:
    ``` 
    mysqldump --skip-lock-tables --single-transaction --flush-logs --master-data=2 <database_name> > database_name.sql
    ```


### Step 4 - Transfer the dump to Slave DB
If you mysqldump file is huge, you can gzip it before transferring to the Slave DB.

1. Zipping the dump file:

    ```
    gzip -9 database_name.sql
    ```

2. Trasfer the mysqldump file to the Slave server.

    ```
    scp database_name.sql.gz <SLAVE_SERVER_USER>@<SLAVE_SERVER_IP>:~/
    ```

### Step 5 - Restore the dump on Slave DB
Login into Server Slave and restore the dump with steps.

1. Create Database on Slave DB with following command.

    ```
    CREATE DATABASE <database_name>;
    ```

2. If you zipped you dump file first extract it with following command.

    ```
    gunzip database_name.sql.gz
    ```

3. Import the dump with following command.

    ```
    mysql -u root -p <database_name> < database_name.sql.gz
    ```

### Step 6 - Configure Slave DB
1. Edit the `/etc/my.cnf` file in Slave server and add the following lines in `[mariadb]` section.

    ```
    [mariadb]
    server-id = 2
    binlog-format = mixed
    log_bin = mysql-bin
    relay-log = mysql-relay-bin
    log-slave-updates = 1
    read-only = 1
    ```

    Users with SUPER privilege (like root) can still write to the database, so you should be careful when granting privileges to users. If you don’t want anyone to be able to change/delete the database, you can add the following line in [mariadb] unit.

    ```
    innodb-read-only = 1
    ```

2. restart the Slave DB to reflect the above configurations
    ```
    sudo systemctl restart mariadb
    ```


### Step 7: Connect the Slave to the Master
1. Login into mysql on Slave DB and connect to Master DB with following command.

    ```
    MariaDB [(none)]> change MASTER to
    -> MASTER_HOST='<master_IP_address>',
    -> MASTER_USER='<master_replicant_user>',
    -> MASTER_PASSWORD=<'master_replicant_user_password>',
    -> MASTER_PORT=3306,
    -> MASTER_LOG_FILE='<master_log_file>',
    -> MASTER_LOG_POS=<master_log_position>
    -> MASTER_CONNECT_RETRY=10,
    // if you setup ssl
    -> MASTER_SSL=1;
    ```

2. Start Slave
   ```
    MariaDB [(none)]> START SLAVE;
    ```

3. Check Slave status
    ```
    MariaDB [(none)]> SHOW SLAVE STATUS\G;
    ```
    If you see no errors in the output, that means replication is running smoothly. You should see the following two “Yes” indicating everything is going well. If one of them is not “Yes”, then something is not right.
    ```
    Slave_IO_Running: Yes
    Slave_SQL_Running: Yes
    ```
