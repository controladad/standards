# Odoo

## installation

setup odoo using this docker-compose file:

```yaml
version: '3.1'
services:
  web:
    image: odoo:16.0
    depends_on:
      - db
    ports:
      - "8069:8069"
    volumes:
      - odoo-web-data:/var/lib/odoo
      - ./config:/etc/odoo
      - ./addons:/mnt/extra-addons
    environment:
      - PASSWORD=<POSTGRESS_PASSWORD>
  db:
    image: postgres:15
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_PASSWORD=<POSTGRESS_PASSWORD>
      - POSTGRES_USER=odoo
      - PGDATA=/var/lib/postgresql/data/pgdata
    volumes:
      - odoo-db-data:/var/lib/postgresql/data/pgdata

volumes:
  odoo-web-data:
  odoo-db-data:
```

* note: replace PASSWORDS with the same password in both places, use `openssl rand -base64 30` to generate a random password

run using `docker-compose up --build -d` and odoo should be accessible from port 8069.
You can set a [reverse proxy](CyberPanel.md#Reverse%20Proxy) with cyberpanel for this port Or use haproxy as following:

1. Install haproxy
2. edit haproxy config file. for apt installations config file located in `etc/haproxy/haproxy.cfg`:
```haproxy
frontend http-in
        bind *:80
        bind *:443 ssl crt /etc/ssl/office.controladad.com/office.controladad.com.pem
        mode http

        use_backend backend_office if { hdr(host) -i office.controladad.com }

backend backend_office 
        mode http
        server odoowebserver localhost:8069
```
3. Restart haproxy service: `service haproxy restart`