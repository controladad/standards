# Container Registry

## Build this repository docker files

Build and push process of this repository is automated by GitLab CI/CD. You can find the CI/CD configuration in `.gitlab-ci.yml` file.

## Build trigger
There's a tag name convention to trigger the build process. The tag name should be in the following format.

`build-image-NUMBER_DOCKERFILENAME`

so for example if you want to build node version 16 image you should create a tag with the following name.

`build-image-100_node16`

Tag patterns are defined in `.gitlab-ci.yml` file.

## Create a new docker file

To create a new docker file you should create a new folder in the root of this repository and create a `Dockerfile` file inside that folder. Then you should add a pattern for that docker file in `.gitlab-ci.yml` file.