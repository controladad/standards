# DevOps Documents

## Table of Contents

|Content|
|-------|
|[ContainerRegistry](ContainerRegistery.md)
|[CyberPanel](CyberPanel.md)|
|[Database](DB)|
|[ELK](ELK.md)|
|[Laravel](Laravel.md)
|[Odoo](Odoo.md)|
|[RocketChat and Jitsi](RocketChat.md)|
|[RabbitMQ](RabbitMQ.md)|
|[Setup New Server](SetupNewServer.md)|