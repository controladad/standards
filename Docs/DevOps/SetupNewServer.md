# Setup New Server
## CyberPanel
It's recommended for servers that mainly serve PHP websites, also there's a possibility for reverse proxy and running other frameworks or applications.

To install CyberPanel use this command on your Linux base server
```bash
sh <(curl https://cyberpanel.net/install.sh || wget -O - https://cyberpanel.net/install.sh)
```
and continue installation wizard.

### Install CSF
After installing cyberpanel, It's better to install CSF for better security, you can install it throw `Secutiry->CSF` menu.

