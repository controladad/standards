# Contribution Guide

## Jira Workflow

***

## Git Workflow

All developments must be pushed in Feature/Task/Story/Bug branches, after code review, they will be merge by maintainers of the project into main branch.

### Branching

>`main`: Not allowed to force push or even push by maintainers all Task/Story/Bug/Feature branches after code review by a maintainer will merge into this branch.
Also, QA/QC department will use this branch to test features and bug fixes.

>`release`: This branch is long-lived. For each major version, a realease developer will create this branch and it lives till major version increases.
>
>This branch will **be not** merge into `main` branch.
>
>When a release developer wants to make a release, he will merge a dev-tag (tagged on master) into this branch then do final release works and tag this branch as a version tag

## Tagging



### Story/Task/Bug branch

### Code Review

### PR/MR
