# Standards

This repository contains documents and container registries for internal use.

***

## Table of Contents

| Content            |
| ------------------ |
| [Documents](Docs/) |
| [Container Registry](Docs/DevOps/ContainerRegistery.md) |

## TODO

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:83aad8c26c5fd5f6207a3d4c45059d40?https://docs.gitlab.com/ee/user/project/members/)

- [ ] Contribution Guide for projects Template
  - [ ] Jira WorkFlow
  - [ ] Git WorkFlow (hotfix-devtag-release-tag)
  - [ ] Task/Bug/Story Branches
  - [ ] Code Review
    - [ ] How to review
    - [ ] Comments
    - [ ] MR/PR Format
- [ ] CyberPanel Upgrade Guide
  - [ ] Before Update CyberPanel
    - [ ] Update System
    - [ ] After Update CyberPanel
    - [ ] Check PHP-CLI version

- [ ]  Maria DB Replication
    -[ ] SSL Config

## Support

If you have questions about any documents, codes, containers, etc., ask one of the maintainers.

## Contributing

If you feel the need to add/update additional codes, documents, etc., please submit a Merge Request(MR/PR).
